/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   filler.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 14:45:41 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/31 18:49:58 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# include "../libft/includes/libft.h"
# include "../libft/includes/ft_printf.h"
# include "../libft/includes/get_next_line.h"

# define POS_X lst->pos_x
# define POS_Y lst->pos_y
# define LETTER lst->letter
# define MAP lst->map
# define PIECE lst->piece
# define NB_STAR lst->nb_star
# define ADV lst->adv
# define ADV_PIECE lst->adv_piece
# define MY_PIECE lst->my_piece
# define FINAL_X lst->final_x
# define FINAL_Y lst->final_y

typedef struct	s_filler
{
	int		pos_x;
	int		pos_y;
	char	letter;
	char	**map;
	char	**piece;
	int		nb_star;
	int		adv;
	int		adv_piece[2];
	int		my_piece[2];
	int		final_x;
	int		final_y;
}				t_fil;

t_fil			*fil_struct_init(void);
void			fil_struct_zero(t_fil *lst);
int				main_managerfil(t_fil *lst);
void			filler_error(t_fil *lst);
void			fil_putinfo(t_fil *lst, int i);
void			fil_analyse_adv(t_fil *lst);

#endif
