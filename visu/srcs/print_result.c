/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_result.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 20:30:11 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:27:34 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

static int	print_player2(t_filvisu *lst, int i)
{
	int ret;

	ret = 0;
	if (i == 6 && WINNER == 1)
		RET_PF("%30c LOOSER IS %{T_BLUE}%s", ' ', PLAYER_2) - 5;
	else if (i == 6 && WINNER == 2)
		RET_PF("%30c LOOSER IS %{T_LGREY}%s", ' ', PLAYER_1) - 5;
	if (i == 7 && WINNER == 1)
		RET_PF("  %40s %{T_RED}%d", "score", SCORE_2) - 11;
	else if (i == 7 && WINNER == 2)
		RET_PF("  %40s %{T_RED}%d", "score", SCORE_1) - 11;
	ft_printf("%{T_EOC}");
	return (ret);
}

static int	print_player(t_filvisu *lst, int i)
{
	int ret;

	ret = 0;
	if (i == 2 && WINNER == 1)
		RET_PF("%10c WINNER IS %{T_LGREY}%s", ' ', PLAYER_1) - 5;
	else if (i == 2 && WINNER == 2)
		RET_PF("%10c WINNER IS %{T_BLUE}%s", ' ', PLAYER_2) - 5;
	if (i == 3 && WINNER == 1)
		RET_PF("  %20s %{T_GREEN}%d", "score", SCORE_1) - 11;
	else if (i == 3 && WINNER == 2)
		RET_PF("  %20s %{T_GREEN}%d", "score", SCORE_2) - 11;
	else if (i == 3 && WINNER == 3)
		RET_PF(" %{T_YELLOW}%38s", "EQUALITY !") - 5;
	if (i == 4 && WINNER == 3)
		RET_PF(" %{T_BLUE}%30s %{T_LGREY}%20s", PLAYER_1, PLAYER_2) - 10;
	if (i == 5 && WINNER == 3)
	{
		RET_PF(" %25s %{T_YELLOW}%-6d%{T_EOC}  ", "score", SCORE_1);
		RET_PF("%8s %{T_YELLOW} %d", "score", SCORE_2) - 14;
	}
	ret += print_player2(lst, i);
	return (ret);
}

static void	print_winner(t_filvisu *lst)
{
	int i;
	int j;

	i = 0;
	j = 0;
	print_border(lst);
	while (i < 10)
	{
		j = ft_printf("|   ");
		j += print_trophy(i, 0);
		while (j < 20)
			j += ft_printf(" ");
		if (i >= 2 && i <= 8)
			j += print_player(lst, i);
		while (j < 105)
			j += ft_printf(" ");
		ft_printf("|\n");
		i++;
	}
	print_border(lst);
}

void		print_result(t_filvisu *lst)
{
	int i;
	int j;

	i = 0;
	print_border(lst);
	while (i < 8)
	{
		j = 0;
		ft_printf("|");
		while (j++ < 100)
		{
			if (j <= 35 || j >= 40)
				j += ft_printf(" ");
			else
				j += print_end(i, j);
		}
		ft_printf("|\n");
		i++;
	}
	print_border(lst);
	print_winner(lst);
}
