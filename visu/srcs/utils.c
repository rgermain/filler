/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utils.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 18:57:27 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:08:23 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

void	fil_visuerror(t_filvisu *lst, char **line)
{
	ft_printf("%{T_RED}");
	ft_printf(" _______  _______  _______  _______  _______ \n");
	ft_printf("(  ____ \\(  ____ )(  ____ )(  ___  )(  ____ )\n");
	ft_printf("| (    \\/| (    )|| (    )|| (   ) || (    )|\n");
	ft_printf("| (__    | (____)|| (____)|| |   | || (____)|\n");
	ft_printf("|  __)   |     __)|     __)| |   | ||     __)\n");
	ft_printf("| (      | (\\ (   | (\\ (   | |   | || (\\ (   \n");
	ft_printf("| (____/\\| ) \\ \\__| ) \\ \\__| (___) || ) \\ \\__\n");
	ft_printf("(_______/|/   \\__/|/   \\__/(_______)|/   \\__/\n");
	ft_printf("%{T_EOC}");
	ft_printf("\n%15s  ", *line);
	gnl_filler(line);
	ft_printf("%s\n\n", *line);
	free(*line);
	filler_free(lst);
	exit(0);
}

void	gnl_filler(char **line)
{
	free(*line);
	get_next_line(0, line);
}

void	filler_free(t_filvisu *lst)
{
	if (lst != NULL)
	{
		if (PLAYER_1 != NULL)
			free(PLAYER_1);
		if (PLAYER_2 != NULL)
			free(PLAYER_2);
		free(lst);
	}
}

char	*put_name(char *player)
{
	char	*tmp;
	int		i;

	i = ft_strlen(player);
	while (player[i] != '/' && i > 0)
		i--;
	tmp = ft_strsub(player, i + 1, ft_strlen(player) - i - 2);
	return (tmp);
}

void	collect_score(t_filvisu *lst, char **line)
{
	SCORE_1 = ft_atoi(*line + 9);
	gnl_filler(line);
	if (PLAYER_2 != NULL && line != NULL)
		SCORE_2 = ft_atoi(*line + 9);
}
