/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_result.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 20:30:11 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/14 21:04:19 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

int	print_end2(int i, int j, int ret)
{
	if (i == 3)
		ret += ft_printf("| (____)|| (__    | (_____ | |   | || |");
	if (i == 3)
		ret += ft_printf("      | |   | (___) |   | |   ");
	if (i == 4)
		ret += ft_printf("|     __)|  __)   (_____  )| |   | || |");
	if (i == 4)
		ret += ft_printf("      | |   |  ___  |   | |   ");
	if (i == 5)
		ret += ft_printf("| (\\ (   | (            ) || |   | || |      | |");
	if (i == 5)
		ret += ft_printf("   | (   ) |   | |   ");
	if (i == 6)
		ret += ft_printf("| ) \\ \\__| (____/\\/\\____) || (___) || ");
	if (i == 6)
		ret += ft_printf("(____/\\| |   | )   ( |   | |   ");
	if (i == 7)
		RET_PF("|/   \\__/(_______/\\_______)(_______)(_______/");
	if (i == 7)
		ret += ft_printf(")_(   |/     \\|   )_(");
	return (ret);
}

int	print_end(int i, int j)
{
	int ret;

	ret = 0;
	if (i >= 0 && i <= 7)
		ft_printf("%{T_LGREY}");
	if (i == 0)
		ret += ft_printf(" _______  _______  _______           _   ");
	if (i == 0)
		ret += ft_printf(" _________ _______ _________");
	if (i == 1)
		ret += ft_printf("(  ____ )(  ____ \\(  ____ \\|\\     /|( \\   \\");
	if (i == 1)
		ret += ft_printf("__   __/(  ___  )\\__   __/");
	if (i == 2)
		ret += ft_printf("| (    )|| (    \\/| (    \\/| )   ( || (      )");
	if (i == 2)
		ret += ft_printf(" (   | (   ) |   ) (   ");
	else
		ret = print_end2(i, j, ret);
	if (i >= 0 && i <= 7)
		ft_printf("%{T_EOC}");
	while (((ret + (j / 2)) + 1) < 105)
		ret += ft_printf(" ");
	return (ret);
}

int	print_trophy(int i, int ret)
{
	if (i <= 9)
		ft_printf("%{T_YELLOW}");
	if (i == 0)
		RET_PF("  ___________");
	else if (i == 1)
		RET_PF(" '._==_==_=_.'");
	else if (i == 2)
		RET_PF(" .-\\:      /-.");
	else if (i == 3)
		RET_PF("| (|:.     |) |");
	else if (i == 4)
		RET_PF(" '-|:.     |-'");
	else if (i == 5)
		RET_PF("   \\::.    /");
	else if (i == 6)
		RET_PF("    '::. .'");
	else if (i == 7)
		RET_PF("      ) (");
	else if (i == 8)
		RET_PF("    _.' '._");
	else if (i == 9)
		RET_PF("`\"\"\"\"\"\"\"\"\"\"\"\"\"`");
	if (i <= 9)
		ft_printf("%{T_EOC}");
	return (ret);
}
