/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_header.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 15:26:00 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:55:51 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

void		print_border(t_filvisu *lst)
{
	int i;

	i = 0;
	ft_printf(" ");
	while (i++ < 104)
		ft_printf("_");
	ft_printf("\n");
}

static int	print_header(int i)
{
	int ret;

	ret = 0;
	if (i >= 0 && i <= 7)
		ft_printf("%{T_RED}");
	if (i == 0)
		ret = ft_printf(" _______ _________ _______          _________");
	else if (i == 1)
		ret = ft_printf("(  ____ \\\\__   __/(  ____ \\|\\     /|\\__   __/");
	else if (i == 2)
		ret = ft_printf("| (    \\/   ) (   | (    \\/| )   ( |   ) (");
	else if (i == 3)
		ret = ft_printf("| (__       | |   | |      | (___) |   | |");
	else if (i == 4)
		ret = ft_printf("|  __)      | |   | | ____ |  ___  |   | |");
	else if (i == 5)
		ret = ft_printf("| (         | |   | | \\_  )| (   ) |   | |");
	else if (i == 6)
		ret = ft_printf("| )      ___) (___| (___) || )   ( |   | |");
	else if (i == 7)
		ret = ft_printf("|/       \\_______/(_______)|/     \\|   )_(");
	if (i >= 0 && i <= 7)
		ft_printf("%{T_EOC}");
	return (ret);
}

static int	header_player2(t_filvisu *lst, int i, int j)
{
	int ret;

	ret = 0;
	if ((i == 5 || i == 6) && j > 40)
	{
		if (i == 5)
			ret = ft_printf("%{T_BLUE}%15s%{T_EOC}", "PLAYER 2") - 9;
		else
			ret = ft_printf("%20s", PLAYER_2);
	}
	if ((ret + j) >= 40)
	{
		while ((ret + j) <= 105)
			ret += ft_printf(" ");
	}
	else
		ret = ft_printf("%30c", ' ');
	return (ret);
}

static int	header_player1(t_filvisu *lst, int i, int j)
{
	int ret;

	ret = 0;
	if ((i == 2 || i == 3) && j < 40)
	{
		if (i == 2)
		{
			ret = ft_printf("%{T_LGREY}%15s%{T_EOC}", "PLAYER 1") - 9;
			ret += ft_printf("%*c", 30 - ret, ' ');
		}
		else
		{
			ret = ft_printf("%20s", PLAYER_1);
			ret += ft_printf("%*c", 30 - ret, ' ');
		}
	}
	else if (j >= 40)
	{
		while (j++ <= 105)
			ret += ft_printf(" ");
	}
	else
		ret = ft_printf("%30c", ' ');
	return (ret);
}

void		print_visufiller(t_filvisu *lst, char *line)
{
	int i;
	int j;

	i = 0;
	print_border(lst);
	while (i < 8)
	{
		j = 0;
		ft_printf("|");
		while (j++ <= 105)
		{
			if (j <= 30 || j >= 40)
			{
				if (i < 4)
					j += header_player1(lst, i, j);
				else
					j += header_player2(lst, i, j);
			}
			else
				j += print_header(i);
		}
		ft_printf(" |\n");
		i++;
	}
	print_border(lst);
}
