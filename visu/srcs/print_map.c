/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_map.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 17:33:22 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:48:15 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

static int	print_map2(char *tmp, int i)
{
	ft_printf("%{B_BLACK}");
	if (tmp[i] == '.')
		ft_printf(".");
	else if (tmp[i] == 'x')
		ft_printf("%{T_BLUE}%C%{T_EOC}", 0x25a2);
	else if (tmp[i] == 'X')
		ft_printf("%{T_BLUE}%C%{T_EOC}", 0x25fc);
	else if (tmp[i] == 'o')
		ft_printf("%{T_LGREY}%C%{T_EOC}", 0x25a2);
	else if (tmp[i] == 'O')
		ft_printf("%{T_LGREY}%C%{T_EOC}", 0x25fc);
	return (1);
}

static void	print_map(t_filvisu *lst, char **line, int i)
{
	char	*tmp;
	int		total;

	print_border(lst);
	while (ft_strstr(*line, "Piece") == NULL)
	{
		tmp = ft_strchr(*line, ' ');
		i = 0;
		total = ABS(((105 - ft_strlen(tmp)) / 2));
		total += ((105 - ft_strlen(tmp)) % 2);
		total -= ft_printf("|");
		while (i < (total))
			i += ft_printf(" ");
		i = 0;
		while (tmp[i] != '\0')
			i += print_map2(tmp, i);
		ft_printf("%{B_EOC}");
		while ((total + i) < 105)
			i += ft_printf(" ");
		ft_printf("|\n");
		i = 0;
		gnl_filler(line);
	}
	print_border(lst);
}

static void	pass_piece(t_filvisu *lst, char **line, int i)
{
	if (ft_strstr(*line, "Piece") != NULL)
	{
		i = 6;
		MAP_X = 0;
		while (ft_isdigit((*line)[i]))
			MAP_X = (MAP_X * 10) + (*line)[i++] - '0';
		while (ft_strstr(*line, "got") == NULL &&
				ft_strstr(*line, "fin") == NULL)
			gnl_filler(line);
		if (ft_strstr(*line, "Player with") != NULL)
			gnl_filler(line);
		if (ft_strstr(*line, " fin: ") != NULL)
			collect_score(lst, line);
	}
}

void		print_combat(t_filvisu *lst, char **line, int i)
{
	ft_printf("\33[2J\33[H");
	print_visufiller(lst, *line);
	if (ft_strstr(*line, "launched") != NULL)
	{
		gnl_filler(line);
		PLAYER_2 = put_name(*line);
		gnl_filler(line);
	}
	if (ft_strstr(*line, "Plateau") != NULL)
	{
		MAP_X = ft_atoi((*line) + 8);
		gnl_filler(line);
		gnl_filler(line);
		print_map(lst, line, 0);
	}
	pass_piece(lst, line, 0);
	if ((*line) != NULL)
		free(*line);
	MAP_X = 0;
}
