/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 15:02:56 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:08:08 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler_visu.h"

static void	init_structvisu(t_filvisu *lst)
{
	PLAYER_1 = NULL;
	PLAYER_2 = NULL;
	SCORE_1 = -1;
	SCORE_2 = -1;
	MAP_X = 0;
	WINNER = 3;
}

static void	put_playername(t_filvisu *lst)
{
	int		i;
	char	*line;

	i = 6;
	while (i > 0)
	{
		get_next_line(0, &line);
		if (ft_strstr(line, "error"))
			fil_visuerror(lst, &line);
		free(line);
		i--;
	}
	get_next_line(0, &line);
	PLAYER_1 = put_name(line);
	free(line);
}

static void	find_winner(t_filvisu *lst)
{
	if (SCORE_1 > SCORE_2)
		WINNER = 1;
	else if (SCORE_1 < SCORE_2)
		WINNER = 2;
	if (PLAYER_1 == NULL)
		PLAYER_1 = ft_strdup("\"(no player)\"");
	if (PLAYER_2 == NULL)
		PLAYER_2 = ft_strdup("\"(no player)\"");
}

int			main(void)
{
	t_filvisu	*lst;
	char		*line;

	if (!(lst = (t_filvisu*)malloc(sizeof(t_filvisu))))
		exit(0);
	init_structvisu(lst);
	put_playername(lst);
	while (SCORE_1 == -1 && SCORE_2 == -1)
	{
		get_next_line(0, &line);
		if (ft_strstr(line, " fin: "))
			collect_score(lst, &line);
		else
		{
			print_combat(lst, &line, 0);
			usleep(35000);
		}
	}
	find_winner(lst);
	print_result(lst);
	filler_free(lst);
	return (0);
}
