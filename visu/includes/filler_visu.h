/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   filler.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 14:45:41 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:27:51 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FILLER_VISU_H
# define FILLER_VISU_H
# include "../../libft/includes/libft.h"
# include "../../libft/includes/ft_printf.h"
# include "../../libft/includes/get_next_line.h"

# define PLAYER_1 lst->name_player1
# define PLAYER_2 lst->name_player2
# define SCORE_1 lst->score_1
# define SCORE_2 lst->score_2
# define MAP_X lst->map_x
# define RET_PF ret += ft_printf
# define WINNER lst->winner

typedef struct	s_filvisu
{
	char	*name_player1;
	char	*name_player2;
	int		score_1;
	int		score_2;
	int		map_x;
	int		winner;
}				t_filvisu;

void			print_visufiller(t_filvisu *lst, char *line);
void			print_result(t_filvisu *lst);
void			print_combat(t_filvisu *lst, char **line, int i);
char			*put_name(char *player);
void			filler_free(t_filvisu *lst);
void			gnl_filler(char **line);
int				print_end(int i, int j);
int				print_trophy(int i, int ret);
void			collect_score(t_filvisu *lst, char **line);
void			fil_visuerror(t_filvisu *lst, char **line);
void			print_border(t_filvisu *lst);

#endif
