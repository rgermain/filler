/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   place_piece.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:42:07 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 17:07:32 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

static void	fil_best_solution(t_fil *lst)
{
	if (((POS_X > FINAL_X || POS_Y > FINAL_Y) && ADV == 11) ||
		((POS_X > FINAL_X || POS_Y < FINAL_Y) && ADV == 9) ||
		((POS_X < FINAL_X || POS_Y > FINAL_Y) && ADV == -11) ||
		((POS_X < FINAL_X || POS_Y < FINAL_Y) && ADV == -9))
	{
		FINAL_X = POS_X;
		FINAL_Y = POS_Y;
	}
	else if (FINAL_X == -1 || FINAL_Y == -1)
	{
		FINAL_X = POS_X;
		FINAL_Y = POS_Y;
	}
}

static int	fil_checkplace(t_fil *lst, int i, int j, int star)
{
	int place;

	place = 0;
	while (MAP[POS_X + i] != NULL && PIECE[i] != NULL)
	{
		j = 0;
		while (MAP[POS_X + i][POS_Y + j] != '\0' && PIECE[i][j] != '\0')
		{
			if (MAP[POS_X + i][POS_Y + j] == '.' && PIECE[i][j] == '*')
				star++;
			else if (((MAP[POS_X + i][POS_Y + j] == LETTER) ||
						MAP[POS_X + i][POS_Y + j] == (LETTER + 32)) &&
					PIECE[i][j] == '*')
				place++;
			else if (PIECE[i][j] != '.')
				return (-1);
			j++;
		}
		i++;
	}
	if (place != 1 || star != (NB_STAR - 1))
		return (-1);
	return (1);
}

static void	fil_place(t_fil *lst)
{
	while (MAP[POS_X] != NULL)
	{
		POS_Y = 0;
		while (MAP[POS_X][POS_Y] != '\0')
		{
			if (fil_checkplace(lst, 0, 0, 0) == 1)
				fil_best_solution(lst);
			POS_Y++;
		}
		POS_X++;
	}
}

int			main_managerfil(t_fil *lst)
{
	fil_struct_zero(lst);
	fil_putinfo(lst, 0);
	fil_analyse_adv(lst);
	fil_place(lst);
	if (FINAL_X == -1 || FINAL_Y == -1)
		return (-1);
	MY_PIECE[0] = FINAL_X;
	MY_PIECE[1] = FINAL_Y;
	return (1);
}
