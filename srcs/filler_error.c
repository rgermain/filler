/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   filler_error.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:30:57 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 18:19:54 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

void	filler_error(t_fil *lst)
{
	if (lst != NULL)
	{
		if (MAP != NULL)
			ft_memdeltab(MAP);
		if (PIECE != NULL)
			ft_memdeltab(MAP);
		free(lst);
	}
	ft_printf("%1@\n", "error", "filler", "can't allocate memory");
}
