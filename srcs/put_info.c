/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   read_manager.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:04:10 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 17:16:39 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

static void	calc_star(t_fil *lst, int i, int j)
{
	while (PIECE[i] != NULL)
	{
		j = 0;
		while (PIECE[i][j] != '\0')
		{
			if (PIECE[i][j] == '*')
				NB_STAR++;
			j++;
		}
		i++;
	}
}

static void	put_piece(t_fil *lst, char *line, int i)
{
	int piece_x;

	piece_x = 0;
	get_next_line(0, &line);
	if (ft_strstr(line, "Piece") != NULL)
		piece_x = ft_atoi(line + 6);
	if (!(PIECE = (char**)malloc(sizeof(char*) * piece_x + 1)))
		filler_error(lst);
	free(line);
	while (i < piece_x)
	{
		get_next_line(0, &line);
		PIECE[i++] = line;
	}
	PIECE[i] = NULL;
	calc_star(lst, 0, 0);
}

void		fil_putinfo(t_fil *lst, int i)
{
	char	*line;
	int		map_x;

	map_x = 0;
	get_next_line(0, &line);
	if (ft_strstr(line, "Plateau") != NULL)
		map_x = ft_atoi(line + 8);
	free(line);
	if (!(MAP = (char**)malloc(sizeof(char*) * map_x + 1)))
		filler_error(lst);
	get_next_line(0, &line);
	free(line);
	while (i < map_x)
	{
		get_next_line(0, &line);
		if (!(MAP[i] = ft_strdup(ft_strchr(line, ' ') + 1)))
			filler_error(lst);
		free(line);
		i++;
	}
	MAP[i] = NULL;
	put_piece(lst, line, 0);
}
