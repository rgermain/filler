/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 14:47:51 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 17:13:59 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

void	fil_struct_zero(t_fil *lst)
{
	if (MAP != NULL)
		ft_memdeltab(MAP);
	if (PIECE != NULL)
		ft_memdeltab(PIECE);
	POS_X = 0;
	POS_Y = 0;
	MAP = NULL;
	PIECE = NULL;
	NB_STAR = 0;
	FINAL_X = -1;
	FINAL_Y = -1;
}

t_fil	*fil_struct_init(void)
{
	t_fil *lst;

	if (!(lst = (t_fil*)ft_memalloc(sizeof(t_fil))))
		filler_error(lst);
	LETTER = 0;
	ADV = 0;
	ADV_PIECE[0] = 0;
	ADV_PIECE[1] = 0;
	MY_PIECE[0] = 0;
	MY_PIECE[1] = 0;
	return (lst);
}
