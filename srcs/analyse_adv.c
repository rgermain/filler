/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   analyse_adv.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/08 20:45:08 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 17:04:22 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

static void	fil_putalgo(t_fil *lst, int i, int j)
{
	ADV_PIECE[0] = i;
	ADV_PIECE[1] = j;
	if (ADV_PIECE[0] >= MY_PIECE[0])
		ADV = 10;
	else
		ADV = -10;
	if (ADV_PIECE[1] >= MY_PIECE[1])
		ADV = (ADV < 0 ? ADV - 1 : ADV + 1);
	else
		ADV = (ADV < 0 ? ADV + 1 : ADV - 1);
}

void		fil_analyse_adv(t_fil *lst)
{
	int		i;
	int		j;
	int		mem;
	char	c_maj;

	i = 0;
	mem = -1;
	c_maj = (LETTER == 'O' ? 'X' : 'O');
	while (MAP[i] != NULL && mem == -1)
	{
		j = 0;
		while (MAP[i][j] != '\0' && mem == -1)
		{
			if (MAP[i][j] == c_maj)
				mem = i;
			else
				j++;
		}
		i++;
	}
	fil_putalgo(lst, i, j);
}
