/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 14:47:51 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/15 17:06:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "filler.h"

static void	put_info_player(t_fil *lst)
{
	char *line;

	get_next_line(0, &line);
	if (ft_strstr(line, "$$$") != NULL)
	{
		if (ft_strstr(line, "rgermain.filler") != NULL &&
				ft_strstr(line, "p1") != NULL)
			LETTER = 'O';
		else
			LETTER = 'X';
		free(line);
	}
}

int			main(void)
{
	t_fil *lst;

	lst = fil_struct_init();
	put_info_player(lst);
	while (main_managerfil(lst) == 1)
		ft_printf("%d %d\n", FINAL_X, FINAL_Y);
	return (0);
}
