# Filler :heavy_check_mark:

## Visualisation

<p align="center">
    <img src="/readme_img/filler.png" width="40%" height="40%" title="visualisation whitout sort">
    <img src="/readme_img/filler2.png" width="40%" height="40%" title="visualisation white sort">
</p>


## Objectifs

Ecrire un joueur de Filler est un défi algoritmique très intéressant. A chaque tour,
le joueur actif reçoit l’état de la grille et doit maximiser ses points tout en tentant de
minimiser ceux de l’adversaire en l’éliminant le plus vite possible.

Les objectifs de ce projet rassemblent toujours les objectifs usuels des projets de dé-
but de cursus : rigueur, pratique duCet pratique d’algorithmes élémentaires. Mais à la
différence d’un jeu plus simple comme le Fillit, il ne s’agit plus de simplement agencer
ses pièces le plus efficacement possible, mais maintenant d’empêcher son adversaire de
le faire ! Il vous faudra donc créer votre propre algorithme de remplissage pour contrer
l’algorithme ennemi.

## Le but du jeu

•Deux adversaires s’affrontent à tour de rôle.

•L’objectif est de gagner le plus de points en remplissant le plateau de jeu avec leplus de pièces possible.

•Le plateau est défini parXcolonnes etYlignes. Il fera doncX x Ycases.

•A chaque tour, le programme du joueur actif se voit fournir l’état actuel du plateauet une pièce à placer.

•La VM représente les pièces posées par un des programmes joueurs avec des cha-ractères’X’et celles posées par l’autre avec des charactères’O’.

•Une pièce est définie parxcolonnes etylignes. Dans chaque pièce, une formed’une ou plusieurs cases est représentée avec le charactère’*’.

•Pour pouvoir poser une pièce, il faut qu’une case de la forme recouvre une case,et une seule case, d’une forme précédemment posée.

•La forme doit rentrer intégralement dans le plateau.

•Le plateau contient une première forme pour chaque programme joueur pour initierla partie.

•La partie s’arrête à la première erreur : dès qu’une pièce ne peut plus être poséeou a été mal posée.

•La VM calcule ensuite le score pour chacun des deux programmes joueurs. Le scorele plus élevé remporte la partie.

## Le Plateau

Un plateau est donc une grille de deux dimensions avec un nombre de lignes et decolonnes arbitraires. Pour lancer la partie un plateau initial doit être passée en argumentà la VM. Ce plateau initial doit comporter une forme de départ pour chaque joueur.Voici un exemple de plateau initial de 14 par 30 :
```
Plateau 14 30:
    012345678901234567890123456789
000 ..............................
001 ..............................
002 ..X...........................
003 ..............................
004 ..............................
005 ..............................
006 ..............................
007 ..............................
008 ..............................
009 ..............................
010 ..............................
011 ...........................O..
012 ..............................
013 ..............................
```

## Les pièces

Les pièces sont générées aléatoirement par la VM. Vous ne pouvez pas prévoir leurtaille ni leur forme avant que la VM ne les transmettent à votre programme. Voiçi quelquesexemples arbitraires de pièces possibles pour vous donner une idée :

```
Piece 4 7   Piece 4 5:  Piece 3 6:
...*...     .**..       .****.
...*...     .***.       **....
...*...     ..*..       *.....
..***..     .....
```

## Le programme joueur

Vous comprenez donc maintenant que le but du projet Fille rest d’écrire un pro-gramme joueur capable de battre son adversaire. Ce programme joueur est passé enparamètre à la VM qui se chargera d’exécuter chaque programme joueur, puis de luitransmettre les informations nécessaires au calcul du prochain coup à chaque tour. Achaque tour, votre programme joueur devra donc :

•Lire le plateau et les pièces sur l’entrée standard. Ces informations proviennentbien évidemment de la VM.

•Ecrire sur la sortie standard les coordonnées pour placer la pièce. Le format serasous la forme suivante :X Y\n".


# usage VM

```
>./filler_vm, made by Hcao and Abanlin, version 1.1
Usage: ./filler_vm -f path [-i | -p1 path | -p2 path] [-s | -q | -t time]
-t  --timeset timeout in second
-q  --quietquiet mode
-i  --interactiveinteractive mode(default)
-p1 --player1use filler binary as a first player
-p2 --player2use filler binary as a second player
-f  --fileuse a map file (required)
-s  --seeduse the seed number (initialization random) (man srand)
```

