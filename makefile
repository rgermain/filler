# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/10/01 15:39:03 by rgermain     #+#   ##    ##    #+#        #
#    Updated: 2019/01/15 18:52:48 by rgermain    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME = rgermain.filler

#CFLAGS = -Wall -Werror -Wextra
CFLAGS = -g

INCLUDE = -Iincludes

CINC = includes/filler.h

SRC = main.c main_managerfil.c struct_init.c put_info.c \
		analyse_adv.c filler_error.c 

OBJ = $(SRC:.c=.o)

DSRC = srcs/
DOBJ = objs/

CSRC = $(addprefix $(DSRC),$(SRC))
COBJ = $(addprefix $(DOBJ),$(OBJ))

all: $(NAME)

$(NAME): $(COBJ)
	@make -C libft/ all
	@make -C visu/	all
	@echo "Compilation de l'executable" $(NAME)
	@gcc $? libft/libft.a -o $(NAME)

$(DOBJ)%.o : $(DSRC)%.c $(CINC)
	@mkdir -p $(DOBJ)
	@gcc $(CFLAGS) $(INCLUDE) -c $< -o $@
#	@echo "Compilation de la fonction "$<

clean:
	@mkdir -p $(DOBJ)
	@rm -rf $(DOBJ)
	@make -C libft clean
	@make -C visu clean
	@echo "Suppresion des objects"

fclean: clean
	@rm -f $(NAME)
	@make -C libft fclean
	@make -C visu fclean
	@echo "Suppresion de l'executable "$(NAME)

re: fclean all

.PHONY: all clean fclean
